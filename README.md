# Portmin

**Portmin** is a simple wrapper around FORTRAN **PORT** optimization routines. It was developed as a side-task for our **SEMOPy** package.

## Author

* **Mescheryakov A. Georgy** - [Herrberg](https://bitbucket.org/herrberg) - undegraduate student, SPbSTU

## License

This project is licensed under the MIT License - see the LICENSE.md file for details.